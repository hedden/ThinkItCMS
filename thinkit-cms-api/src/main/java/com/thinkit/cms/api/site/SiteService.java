package com.thinkit.cms.api.site;

import com.thinkit.cms.dto.admin.OrgDto;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.Tree;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 站点管理表 服务类
 * </p>
 *
 * @author LG
 * @since 2020-07-22
 */
public interface SiteService extends BaseService<SiteDto> {


    /**
     * 创建保存站点
     * @param v
     */
    void save(SiteDto v);

    /**
     * 修改站点信息
     * @param v
     */
    void update(SiteDto v);

    /**
     * 设置默认站点
     * @param siteId
     */
    void setDefault(String siteId);

    /**
     * 获取默认站点ID
     * @return
     */
    String getDefault();

    /**
     * 获取默认站点 domain
     * @return
     */
    String getDomain(String schema, String siteId);

    String getDomainCode(String siteId);

    Map<String,Object> getDomainCode(Boolean ssl, String siteId);

    String getDestPath(boolean containCode, String siteId);

    /**
     * 查询首页模板以及生成模板的位置
     * @return
     */
    Map<String,Object> findHomePageFile();

    boolean delete(String id);

    List<SiteDto> getAllSite(SiteDto v, String userId, String orgId);

    /**
     * 获取站点信息数据指令
     * @return
     */
    Map<String,Object> direForSiteInfo();

    Tree<OrgDto> getOrgTres();

    void setOrgDefault(String siteId);
}