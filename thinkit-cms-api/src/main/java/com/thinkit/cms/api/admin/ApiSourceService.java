package com.thinkit.cms.api.admin;

import com.thinkit.cms.dto.admin.ApiSourceDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.Tree;

import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
public interface ApiSourceService extends BaseService<ApiSourceDto> {


    /**
     * 查询资源树
     * @return
     */
    Tree<ApiSourceDto> selectTreeList();


    /**
     * 根据主键删除
     * @param id
     * @return
     */
    boolean deleteById(String id);

    /**
     * 查询客户端资源
     * @param clientId
     * @return
     */
    Tree<ApiSourceDto> selectTreeResourceByClientId(String clientId);

    /**
     * 查询权限标识
     * @param url
     * @param application
     * @return
     */
    Set<String> selectAppPermsByUrl(String url, String application);


    /**
     * 查询用户权限
     * @param clientId
     * @param application
     * @return
     */
    Set<String> selectAppPermsByClient(String clientId, String application);

    void save(ApiSourceDto v);

    void update(ApiSourceDto v);

    ApiSourceDto getById(String id);
}