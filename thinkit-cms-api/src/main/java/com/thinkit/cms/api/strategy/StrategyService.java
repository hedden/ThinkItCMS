package com.thinkit.cms.api.strategy;
import com.thinkit.cms.dto.strategy.StrategyDto;
import com.thinkit.core.base.BaseService;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lg
 * @since 2021-05-12
 */
public interface StrategyService extends BaseService<StrategyDto> {

    List<StrategyDto> queryStrategy(String categoryId,String actionCode,Integer isSatrt);


    void saveStrategy(List<StrategyDto> strategyDtos);

    void clearStrategy(String categoryId);
}