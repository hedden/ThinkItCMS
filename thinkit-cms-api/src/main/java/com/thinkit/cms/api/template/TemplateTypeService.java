package com.thinkit.cms.api.template;
import com.thinkit.cms.dto.template.TemplateTypeDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.Tree;
import java.util.List;

/**
 * <p>
 * 模板分类 服务类
 * </p>
 *
 * @author lg
 * @since 2020-07-28
 */
public interface TemplateTypeService extends BaseService<TemplateTypeDto> {


    /**
     * 查询分类列表树
     * @return
     */
    Tree<TemplateTypeDto> selectTreeList();

    /**
     * 查询详情
     * @param id
     * @return
     */
    TemplateTypeDto getById(String id);

    /**
     * 保存
     * @param v
     */
    void save(TemplateTypeDto v);

    void update(TemplateTypeDto v);

    boolean delete(String id);

    List<TemplateTypeDto> getTempByPidTypes(String pid);

    TemplateTypeDto getByCode(String code);
}