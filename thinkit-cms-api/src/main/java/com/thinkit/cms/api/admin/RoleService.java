package com.thinkit.cms.api.admin;
import com.thinkit.cms.dto.admin.RoleDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.ApiResult;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author dl
 * @since 2018-03-19
 */
public interface RoleService extends BaseService<RoleDto> {

	  ApiResult deleteById(String id);

	  Set<String> selectRoleSignByUid(String userId);

	  List<RoleDto> selectAllRoles();
}
