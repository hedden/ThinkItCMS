package com.thinkit.cms.api.category;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.category.CategoryNavbar;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.KeyValueModel;
import com.thinkit.utils.model.Tree;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 分类 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-07
 */
public interface CategoryService extends BaseService<CategoryDto> {


    Tree<CategoryDto> treeCategory();


    Map<String, Object> loadTemplate();

    /**
     * 置空栏目引用的 模型ID
     * @param categoryModelId
     */
    void deleteCategoryModel(String categoryModelId);

    /**
     * 保存
     * @param v
     */
    ApiResult save(CategoryDto v);

    List<KeyValueModel> loadPathRule();

    /**
     * 更新
     * @param v
     */
    void update(CategoryDto v);

    /**
     * 删除栏目
     * @param id
     * @return
     */
    boolean deleteById(String id);

    /**
     * 获取详情
     * @param id
     * @return
     */
    Map<String,Object> getDetail(String id);

    CategoryDto getDetailById(String id);

    List<CategoryDto> listCategoryByPid(String categoryId);

    /**
     * 栏目树内容发布
     * @return
     */
    Tree<CategoryDto> treeCategoryForContent();

    String getCode(String categoryId);

    List<CategoryNavbar> navbar(String code);

    Map<String, Object> loadTempParams(String categoryId);

    /**
     * 获取栏目详情
     * @param code
     * @param id
     * @return
     */
    Map<String, Object> info(String code, String id);

    /**
     * 获取栏目下的子栏目详情
     * @param id
     * @param id
     * @return
     */
    List<Map<String, Object>> childinfo(String id);

    /**
     * 静态化分类
     * @param id
     */
    void genCategory(String id);

    /**
     * 查询内容所有参数用于生成全部内容
     * @return
     */
    List<String> getIds();

    /**
     * 获取面包屑导航
     * @param id
     * @param postfix
     * @return
     */
    List<CategoryDto>  breadCrumbs(String id, String postfix, Boolean containit);

    Integer queryPageSize(String code, String categoryId);
}