package com.thinkit.cms.api.admin;
import com.thinkit.cms.dto.admin.AnalysisDto;
import com.thinkit.core.base.BaseService;

/**
 * <p>
 * 流量统计配置 服务类
 * </p>
 *
 * @author lg
 * @since 2020-11-25
 */
public interface AnalysisService extends BaseService<AnalysisDto> {



}