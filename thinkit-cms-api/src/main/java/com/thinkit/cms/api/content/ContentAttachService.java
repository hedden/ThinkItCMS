package com.thinkit.cms.api.content;
import com.thinkit.cms.dto.content.ContentAttachDto;
import com.thinkit.core.base.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容附件 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-19
 */
public interface ContentAttachService extends BaseService<ContentAttachDto> {


    List<String> listData(String contentId);

    List<Map> listAttach(String contentId);

    List<Map> listAttachs(List<String> contentIds);
}