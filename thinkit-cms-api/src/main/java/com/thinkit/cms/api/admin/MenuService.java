package com.thinkit.cms.api.admin;

import com.thinkit.cms.dto.admin.MenuDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.Tree;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单管理 服务类
 * </p>
 *
 * @author dl
 * @since 2018-03-21
 */
public interface MenuService extends BaseService<MenuDto> {


	boolean save(MenuDto menu);

	boolean update(MenuDto menu);

	Tree<MenuDto> selectTreeList();

	List<MenuDto> selectMenuTreeByUid(String userId);
	
	Set<String> selectPermsByUid(String userId, String application);

	void clearPermsCacheByUid();

	/**
	* @Description: 根据登录人查询其所拥有的菜单
	* @param @param userId
	* @param @return  
	* @return List<Menu> 
	* @throws 
	*/ 
	List<MenuDto> selectMenuUid(String userId);

    MenuDto info(String id);

	boolean delete(String menuId);

	/**
	 * 加载用户菜单
	 * @param userId
	 * @return
	 */
	List<MenuDto>loadMenu(String userId);

	/**
	 * 加载微服务实例
	 * @return
	 */
    List<String> loadApplications();

	/**
	 * 隐藏菜单
	 * @param id
	 * @param hidden
	 */
	void hideIt(String id, Integer hidden);
}
