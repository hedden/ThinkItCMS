package com.thinkit.cms.dto.fragment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.core.annotation.valid.ValidGroup1;
import com.thinkit.utils.model.BaseFieldDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 页面片段文件模型
 * </p>
 *
 * @author lg
 * @since 2020-08-02
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FragmentModelDto extends BaseFieldDto {

        private static final long serialVersionUID = 1L;



        @NotBlank(message = "路径不能为空",groups = {ValidGroup1.class})
        private String path;

        @NotBlank(message = "相对路径不能为空",groups = {ValidGroup1.class})
        private String relativePath;

        /**
        * 别名
        */
        @NotBlank(message = "片段别名不能为空",groups = {ValidGroup1.class})
        private String alias;


        /**
        * 片段编码
        */
        @NotBlank(message = "片段编码不能为空",groups = {ValidGroup1.class})
        private String code;


        /**
        * 模板id
        */
        @NotBlank(message = "模板ID不能为空",groups = {ValidGroup1.class})
        private String templateId;


        /**
        * 展示条数
        */
        @NotNull(message = "条数不能为空",groups = {ValidGroup1.class})
        private Integer size;


        /**
        * 文件名称
        */
        private String fileName;


}
