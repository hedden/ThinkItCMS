package com.thinkit.cms.dto.site;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 站点管理表
 * </p>
 *
 * @author LG
 * @since 2020-07-22
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SiteDto extends BaseDto {

        private static final long serialVersionUID = 1L;

        /**
        * 站点名称
        */
        @NotBlank(message = "站点名称不能为空")
        private String siteName;


        private Boolean isDefault;

        private Boolean isShareOrg;

        /**
        * 站点副标题
        */
        private String siteSubtitle;


        /**
        * 关键字
        */
        private String siteKeyWords;


        /**
        * 站点描述
        */
        private String siteDesc;


        /**
        * 站点logo
        */
        private String siteLogo;

        /**
         * 组织ID
         */
        private String orgId;


        /**
        * 域名
        */
        @NotBlank(message = "站点域名不能为空")
        private String domain;


        /**
        * 站点备案信息
        */
        private String siteicp;


        /**
        * 站点版权
        */
        private String copyright;


        /**
        * 第三方评论代码
        */
        private String commentCode;


        /**
        * 第三方分享代码
        */
        private String shareCode;


        /**
        * 第三方统计代码
        */
        private String statisticalCode;


        private String site;

}
