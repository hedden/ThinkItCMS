package com.thinkit.cms.dto.content;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.core.annotation.valid.ValidGroup1;
import com.thinkit.utils.annotation.StoreMark;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContentDto extends BaseDto {

        /**
        * 站点ID
        */
        @StoreMark
        private String siteId;


        /**
        * 标题
        */
        @StoreMark
        @NotBlank(message = "标题不能为空",groups = {ValidGroup1.class})
        private String title;


        /**
        * 副标题
        */
        @StoreMark
        private String subTitle;


        /**
        * 审核用户
        */
        private String approveUserId;


        private String approveUserName;


        /**
        * 分类
        */
        @StoreMark
        @NotBlank(message = "分类不能为空",groups = {ValidGroup1.class})
        private String categoryId;


        /**
        * 模型关系
        */
        @NotBlank(message = "模型关系不能为空",groups = {ValidGroup1.class})
        private String modelId;


        private String modelName;

        /**
        * 是否转载
        */
        @StoreMark
        private Boolean copied;


        /**
        * 作者
        */
        @StoreMark
        private String author;


        /**
        * 编辑
        */
        @StoreMark
        private String editor;


        /**
        * 是否置顶
        */
        private Boolean istop;


        /**
        * 是否推荐
        */
        private Boolean isrecomd;


        /**
        * 是否头条
        */
        private Boolean isheadline;


        /**
        * 外链
        */
        @StoreMark
        private Boolean onlyUrl;


        /**
        * 拥有附件列表
        */
        private Boolean hasFiles = false;


        /**
        * 是否包含字附件
        */
        private Boolean hasTags;


        /**
        * 是否有推荐内容
        */
        private Integer hasRelated;


        /**
        * 地址
        */
        @StoreMark
        private String url;


        /**
        * 简介
        */
        @StoreMark
        private String description;


        private String pCategoryId;

        /**
        * 标签
        */
        private String tagIds;


        /**
        * 置顶标签
        */
        private String topTag;

        @StoreMark
        private List<String> topTags;

        /**
        * 封面
        */
        @StoreMark
        private List<Map> cover =new ArrayList<>();

        //@StoreMark
        private String coverStr;

        /**
        * 评论数
        */
        private Integer comments;


        /**
        * 点击数
        */
        private Integer clicks;


        private String tags;


        /**
        * 日期生成规则
        */
        private String pathRule;


        /**
        * 点赞数
        */
        private Integer giveLikes;


        /**
        * 发布日期
        */
        @StoreMark
        private Date publishDate;


        /**
        * 发表用户ID
        */
        private String publishUserId;


        /**
        * 定时发布日期
        */
        private Date jobDate;


        /**
        * 审核日期(c)
        */
        private Date approveDate;


        /**
        * 顺序
        */
        private Integer sort;


        /**
        * 状态：0、草稿 1、已发布 2、删除
        */
        @StoreMark
        private String status;


        /**
        * 创建人名称
        */
        private String createName;


        /**
        * 修改人名称
        */
        private String modifiedName;


       // -------------------扩展------------
        @StoreMark
        private String text;

        private String attrData;

        @StoreMark(expand = true)
        private Map<String,Object> params;

        @StoreMark
        private String categoryName;

        /**
         * 内容来源
         */
        private String origin;

        /**
         * 来源地址
         */
        private String originUrl;

        private String categoryCode;


        private List<String> categoryIds;

        List<String> contentIds;

        private Boolean realDel;
}
