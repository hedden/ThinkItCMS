package com.thinkit.cms.dto.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiSourceDto extends BaseDto {


        private String parentId;


        /**
        * 0:模块 1：接口
        */
        private Integer apiType;


        /**
        * 接口名称
        */
        private String apiName;


        /**
        * 接口地址
        */
        private String apiUrl;


        /**
        * 接口标识
        */
        private String apiPerm;


        /**
        * 接口描述
        */
        private String apiDesc;


        /**
        * 接口所属模块
        */
        private String apiModule;


        /**
         * 接口所属模块
         */
        private String apiOrderNum;

}
