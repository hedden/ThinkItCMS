package com.thinkit.core.annotation;


import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Clear {
	//Class<? extends HandlerInterceptor>[] value() default {};
}