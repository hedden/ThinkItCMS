package com.thinkit.core.handler;
import com.thinkit.utils.model.ApiResult;
import lombok.Data;

@Data
public class CustomException extends RuntimeException  {

	public ApiResult r;

	public int code=-1;

	public CustomException(ApiResult r) {
		super(r.getMessage());
		this.r = r;
		this.code = r.getCode();
	}

	public CustomException(String msg) {
		super(msg);
		this.r = ApiResult.result(-1,msg);
		this.code =-1;
	}

	public CustomException(String msg,int code) {
		super(msg);
		this.code = code;
		this.r = ApiResult.result(code,msg);
	}

}
