package com.thinkit.log.aspect;
import com.thinkit.log.service.LogService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
	  
	  @Autowired
	  private LogService logService;
	
	  @Around("execution(* com.thinkit.*..*.*(..)) && @annotation(com.thinkit.log.annotation.Logs)")
      public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
		    Long starTime = System.currentTimeMillis();
			Object result = pjp.proceed();
		    Long time = System.currentTimeMillis()-starTime;
			logService.saveLog(pjp, time.intValue());//单位毫秒
			return result;
      }
	  
}
