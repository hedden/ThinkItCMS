package com.thinkit.directive.render;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import lombok.extern.slf4j.Slf4j;
import java.util.Map;
@Slf4j
public abstract class BaseDirective implements TemplateDirectiveModel ,Directive{


    @Override
    public void execute(Environment environment, Map map, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException {
        try {
            execute(new RenderWrapper(buildWrapper(environment,map,templateModels,templateDirectiveBody)));
        } catch (ClassCastException e) {
             log.error("~~~~~~~~数据类型转换异常,请确认您的模板参数类型是否正确~~~~~");
             throw new TemplateException(e, environment);
        }catch (Exception e) {
             throw new TemplateException(e, environment);
        }
    }

    private Wrapper buildWrapper(Environment environment,
                                 Map map, TemplateModel[] templateModels,
                                 TemplateDirectiveBody templateDirectiveBody){
        Wrapper wrapper = new Wrapper(environment, map, templateModels, templateDirectiveBody);
        return wrapper;
    }

}
