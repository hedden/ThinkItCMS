package com.thinkit.security.custom;
import com.thinkit.core.constant.Constants;
import com.thinkit.core.constant.SecurityConstants;
import com.thinkit.core.handler.CustomException;
import com.thinkit.security.config.AbsCustomJwtHandler;
import com.thinkit.utils.enums.UserFrom;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Component
public class CustomJwtHandler extends AbsCustomJwtHandler {

    @Override
    public void handlerJwtToken(Authentication authentication , JwtTokenStore tokenStore, HttpServletRequest request ) throws CustomException {
        String tokenValue = authentication.getPrincipal().toString();//读取token信息
        OAuth2Authentication oauth=tokenStore.readAuthentication(tokenValue);
        Map<String, Object> details = (Map<String, Object>)oauth.getDetails();
        String siteId=request.getHeader(Constants.DEFAULT_SITE);
        String clientId=(String)details.get(SecurityConstants.CLIENT_ID);
        details.put(Constants.DEFAULT_SITE,siteId);
        UserFrom userFrom = UserFrom.getUserFrom(clientId);
        if(userFrom.equals(UserFrom.PLAT_USER)){
            super.handPlatUserSession(details,userFrom);
        }else{
            super.handAppUserSession(details,userFrom);
        }
    }
}
