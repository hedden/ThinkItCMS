package com.thinkit.processor.message;

public interface MessageActuator {

    void submit(MessageResolve messageResolve);
}
