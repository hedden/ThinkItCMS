package com.thinkit.utils.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;

/**
 *
 * FileInfo 文件信息封装类
 *
 */
@Data
@Accessors(chain = true)
public class FileViewModel extends FileInfoModel {

      public FileViewModel(){

      }
      public FileViewModel(String fileName, boolean directory, BasicFileAttributes attrs){
         super(fileName,directory,attrs);
      }

      public FileViewModel(String fileName, boolean directory){
            this.fileName = fileName;
            this.directory = directory;
      }

      private String relativePath;


}
