package com.thinkit.utils.properties;
import com.thinkit.utils.utils.Checker;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "permission")
public class PermitAllUrlProperties {

    private List<Application> application=new ArrayList<>();

    private Boolean strict = false;

    public boolean ckTheSameApi(String name,String apiUrl){
        if(Checker.BeBlank(name)){
            return false;
        }
        boolean res=false;
        look:
        for (Application app : this.getApplication()) {
            if (name.equals(app.getAppName())) {
                for (String api : app.getAuthc()) {
                    if (apiUrl.equals(api)) {
                        res = true;
                        break look;
                    }
                }
            }
        }
        return res;
    }

    public  List<String> applications(){
        List<String> appList=new ArrayList<>(16);
        if(Checker.BeNotEmpty(application)){
            application.forEach(app->{
                appList.add(app.getAppName());
            });
        }
        return appList;
    }

    public  List<String> authcs(){
        List<String> authcs=new ArrayList<>(16);
        if(Checker.BeNotEmpty(application)){
            application.forEach(app->{
                authcs.addAll(app.getAuthc());
            });
        }
        return authcs;
    }

    public  List<String> authcs(String appName){
        List<String> authcs=new ArrayList<>(16);
        if(Checker.BeNotEmpty(application)){
            application.forEach(app->{
                if(appName.equals(app.getAppName())){
                    authcs.addAll(app.getAuthc());
                }
            });
        }
        return authcs;
    }

    public  List<String> ignores(){
        List<String> ignores=new ArrayList<>(16);
        if(Checker.BeNotEmpty(application)){
            application.forEach(app->{
                ignores.addAll(app.getIgnore());
            });
        }
        return ignores;
    }

    public  List<String> ignores(String appName){
        List<String> ignores=new ArrayList<>(16);
        if(Checker.BeNotEmpty(application)){
            application.forEach(app->{
                if(appName.equals(app.getAppName())){
                    ignores.addAll(app.getIgnore());
                }
            });
        }
        return ignores;
    }

    public List<Application> getApplication() {
        return application;
    }

    public void setApplication(List<Application> application) {
        this.application = application;
    }

    public Boolean getStrict() {
        return strict;
    }

    public void setStrict(Boolean strict) {
        this.strict = strict;
    }
}
