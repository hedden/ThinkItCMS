package com.thinkit.utils.enums;
import lombok.Getter;

public enum ChannelEnum {
	CONTENT("CONTENT","内容通道"),
	CONTENTS("CONTENTS","内容通道"),
	CATEGORY("CATEGORY","分类通道"),
	CATEGORY_PAGE("CATEGORY_PAGE","分类分页通道"),
	TEMPLATE("TEMPLATE","模板通道"),
	HOME("HOME","首页通道");


	@Getter
	private String code;

	@Getter
	private String name;

	ChannelEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}
}
