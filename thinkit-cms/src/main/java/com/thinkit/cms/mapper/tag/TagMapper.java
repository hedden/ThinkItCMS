package com.thinkit.cms.mapper.tag;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.tag.TagDto;
import com.thinkit.cms.entity.tag.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 标签 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-08-20
 */
@Mapper
public interface TagMapper extends BaseMapper<Tag> {

    List<TagDto> listTags(@Param("tags") List<String> tags,@Param("siteId") String siteId);

    List<String> listTagByIds(@Param("tagIds") List<String> tagIds);

    IPage<TagDto> listPage(IPage<TagDto> pages, @Param("dto") TagDto dto);
}
