package com.thinkit.cms.mapper.site;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.site.UserSiteSettingDto;
import com.thinkit.cms.entity.site.UserSiteSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户站点设置表 Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2020-09-20
 */
@Mapper
public interface UserSiteSettingMapper extends BaseMapper<UserSiteSetting> {

    UserSiteSettingDto getBySiteId(@Param("siteId") String siteId, @Param("userId") String userId);


    void updateDefault( @Param("userId") String userId);

    List<UserSiteSettingDto> getByUserId(@Param("userId") String userId);
}
