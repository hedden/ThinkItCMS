package com.thinkit.cms.mapper.admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.entity.admin.Role;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author dl
 * @since 2018-03-19
 */
@Mapper
@Repository
public interface RoleMapper extends BaseMapper<Role> {

}
