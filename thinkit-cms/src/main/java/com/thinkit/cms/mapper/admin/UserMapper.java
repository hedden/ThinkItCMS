package com.thinkit.cms.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.admin.UserDto;
import com.thinkit.cms.entity.admin.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Mapper
@Repository
public interface UserMapper extends BaseMapper<UserEntity> {

    Set<String> selectRoleSignByUserId(@Param("userId") String uid);

    boolean lockUsers(@Param("pass") String pass, @Param("justLock") Boolean justLock);

    UserDto getUserInfo(@Param("userId") String userId);

    IPage<UserDto> listPage(IPage<UserDto> pages, @Param("dto") UserDto dto, @Param("userId") String userId);

    UserDto checkUserIsExist(@Param("userId") String userId,
                             @Param("tableField") String tableField,
                             @Param("val") String val) ;

    List<String> getUserIdsByOrgId(@Param("orgId") String orgId);


    UserDto loadPlatUserByUsername(@Param("userAccount") String userAccount);

}
