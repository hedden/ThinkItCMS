package com.thinkit.cms.mapper.admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.entity.admin.SysJob;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2019-11-19
 */
@Mapper
public interface SysJobMapper extends BaseMapper<SysJob> {

}
