package com.thinkit.cms.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.admin.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

@Mapper
public  interface PermissionMapper extends BaseMapper {

    /**
     * 查询资源url 和 资源标识的对应关系
     * @return
     */
   List<Permission> selectPlatPermIdsAndUrl();

    /**
     * 查询会员所拥有的全部资源标识
     * @param userId
     * @return
     */
   Set<String> selectPlatPermIdsByUid(@Param("userId") String userId);
}
