package com.thinkit.cms.controller.admin;

import com.thinkit.cms.api.admin.MemberService;
import com.thinkit.cms.dto.admin.MemberDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-11-03
 */
@Validated
@RestController
@RequestMapping("api/member")
public class MemberController extends BaseController<MemberService> {


    @GetMapping("getByPk")
    public MemberDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @PostMapping(value="save")
    public void save(@Validated @RequestBody MemberDto v){
        service.insert(v);
    }

    @PutMapping("update")
    public void update(@RequestBody MemberDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<MemberDto> list(@RequestBody MemberDto v){
        return service.listDto(v);
    }

    @PostMapping("page")
    public PageDto<MemberDto> listPage(@RequestBody PageDto<MemberDto> pageDto){
        return service.listPage(pageDto);
    }

}
