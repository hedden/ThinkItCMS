package com.thinkit.cms.controller.license;

import com.thinkit.cms.api.license.LicenseService;
import com.thinkit.cms.dto.license.LicenseDto;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 内容 前端控制器
 * </p>
 *
 * @author LG
 * @since 2019-10-30
 */
@Validated
@RestController
@RequestMapping("/license")
public class LicenseController {

    @Autowired
    private LicenseService licenseService;

    @GetMapping("page")
    public PageDto<LicenseDto> page(){
        return licenseService.page();
    }


    @PostMapping("/importLicense")
    public ApiResult importLicense(@RequestParam("file") MultipartFile file) {
        return licenseService.importLicense(file);
    }
}
