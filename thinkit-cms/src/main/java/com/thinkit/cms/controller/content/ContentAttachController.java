package com.thinkit.cms.controller.content;

import com.thinkit.cms.api.content.ContentAttachService;
import com.thinkit.cms.dto.content.ContentAttachDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 内容附件 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-19
 */
@Validated
@RestController
@RequestMapping("contentAttach")
public class ContentAttachController extends BaseController<ContentAttachService> {


    @GetMapping("getByPk")
    public ContentAttachDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @PostMapping(value="save")
    public void save(@Validated @RequestBody ContentAttachDto v){
        service.insert(v);
    }

    @PutMapping("update")
    public void update(@RequestBody ContentAttachDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<ContentAttachDto> list(@RequestBody ContentAttachDto v){
        return service.listDto(v);
    }

    @PostMapping("page")
    public PageDto<ContentAttachDto> listPage(@RequestBody PageDto<ContentAttachDto> pageDto){
        return service.listPage(pageDto);
    }

}
