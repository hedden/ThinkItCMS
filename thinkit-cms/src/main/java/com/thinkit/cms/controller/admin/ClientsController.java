package com.thinkit.cms.controller.admin;
import com.thinkit.cms.api.admin.ClientsService;
import com.thinkit.cms.dto.admin.ClientsDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * <p>
 * 终端信息表 前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
@Validated
@RestController
@RequestMapping("clients")
public class ClientsController extends BaseController<ClientsService> {


    @PostMapping("page")
    public PageDto<ClientsDto> listPage(@RequestBody PageDto<ClientsDto> pageDto){
        return service.listPage(pageDto);
    }

    @PutMapping("update")
    public void update(@RequestBody ClientsDto clientDto){
         service.updateSecretByClientId(clientDto);;
    }

    @PostMapping("list")
    public List<ClientsDto> list(@RequestBody ClientsDto v){
        return service.listClients();
    }
}
