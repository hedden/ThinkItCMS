package com.thinkit.cms.controller.template;

import com.thinkit.cms.api.template.TemplateTypeService;
import com.thinkit.cms.dto.template.TemplateTypeDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.model.Tree;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 模板分类 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-07-28
 */
@Validated
@RestController
@RequestMapping("templateType")
public class TemplateTypeController extends BaseController<TemplateTypeService> {


    @GetMapping("getByPk")
    public TemplateTypeDto get(@NotBlank @RequestParam String id){
       return  service.getById(id);
    }

    @PostMapping(value="save")
    public void save(@Validated @RequestBody TemplateTypeDto v){
        service.save(v);
    }

    @PutMapping("update")
    public void update(@Validated @RequestBody TemplateTypeDto v){
        service.update(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.delete(id);
    }

    @GetMapping("getTempByPidTypes")
    public  List<TemplateTypeDto> getTempByPidTypes(@RequestParam(defaultValue = "0") String pid){
        return service.getTempByPidTypes(pid);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<TemplateTypeDto> list(@RequestBody TemplateTypeDto v){
        return service.listDto(v);
    }

    @PostMapping("page")
    public PageDto<TemplateTypeDto> listPage(@RequestBody PageDto<TemplateTypeDto> pageDto){
        return service.listPage(pageDto);
    }

    @RequestMapping("/selectTreeList")
    public Tree<TemplateTypeDto> selectTreeList(){
        Tree<TemplateTypeDto> tempTypes=service.selectTreeList();
        return tempTypes;
    }

}
