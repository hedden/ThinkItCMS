package com.thinkit.cms.controller.apis;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.base.BaseController;
import com.thinkit.core.base.HttpServletKit;
import com.thinkit.core.handler.CustomException;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.model.SolrSearchModel;
import com.thinkit.utils.utils.Checker;
import org.apache.solr.common.SolrDocument;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 浏览网页的 接口请求
 * </p>
 *
 * @author LG
 * @since 2019-10-30
 */
@Validated
@RestController
@RequestMapping("/api/search")
public class ViewPageController extends BaseController<ContentService> {

    /**
     * 浏览点击次数
     * @param cid
     * @return
     */
    @GetMapping("viewClicks")
    public Long viewClicks(@RequestParam String cid){
        return service.viewClicks(cid);
    }


    /**
     * 获取文章的点赞次数
     * @param cid
     * @return
     */
    @GetMapping("viewLikes")
    public ApiResult viewLikes(HttpServletRequest request, @RequestParam String cid){
        String userAgent= HttpServletKit.getIpAddress(request);
        return service.viewLikes(cid,userAgent);
    }



    /**
     * 关键字搜索
     * @param pageDto
     * @return
     */
    @PostMapping("searchKeyWord")
    public PageDto<SolrDocument> searchKeyWord(@RequestBody PageDto<SolrSearchModel> pageDto){
        if(Checker.BeNull(pageDto) || Checker.BeNull(pageDto.getDto())|| pageDto.getPageSize()>100){
            throw  new CustomException(ApiResult.result(20018));
        }
        return service.searchKeyWord(pageDto);
    }







}
