package com.thinkit.cms.controller.site;

import com.thinkit.cms.api.site.UserSiteSettingService;
import com.thinkit.cms.dto.site.UserSiteSettingDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
/**
 * <p>
 * 用户站点设置表 前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-09-20
 */
@Validated
@RestController
@RequestMapping("userSiteSetting")
public class UserSiteSettingController extends BaseController<UserSiteSettingService> {


    @GetMapping("getByPk")
    public UserSiteSettingDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @PostMapping(value="save")
    public void save(@Validated @RequestBody UserSiteSettingDto v){
        service.insert(v);
    }

    @PutMapping("update")
    public void update(@RequestBody UserSiteSettingDto v){
        service.updateByPk(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<UserSiteSettingDto> list(@RequestBody UserSiteSettingDto v){
        return service.listDto(v);
    }

    @PostMapping("page")
    public PageDto<UserSiteSettingDto> listPage(@RequestBody PageDto<UserSiteSettingDto> pageDto){
        return service.listPage(pageDto);
    }

}
