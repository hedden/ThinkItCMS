package com.thinkit.cms.controller.template;

import com.thinkit.cms.api.site.SiteTemplateService;
import com.thinkit.cms.dto.site.SiteTemplateDto;
import com.thinkit.core.base.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 站点模型表 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Validated
@RestController
@RequestMapping("siteTemplate")
public class SiteTemplateController extends BaseController<SiteTemplateService> {



    @PostMapping(value="inunstall")
    public void save(@Validated @RequestBody SiteTemplateDto v){
        service.inunstall(v);
    }

}
