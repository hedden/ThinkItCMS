package com.thinkit.cms.entity.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 流量统计配置
 * </p>
 *
 * @author lg
 * @since 2020-11-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_analysis")
public class Analysis extends BaseModel {


    /**
     * 统计平台
     */

        @TableField("analysis_plat")
        private String analysisPlat;


    /**
     * pc端地址
     */

        @TableField("pc_url")
        private String pcUrl;


    /**
     * 手机端地址
     */

        @TableField("mobile_url")
        private String mobileUrl;


    /**
     * 0:非默认 1：默认
     */

        @TableField("is_default")
        private Integer isDefault;



}
