package com.thinkit.cms.entity.fragment;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 页面片段文件模型
 * </p>
 *
 * @author lg
 * @since 2020-08-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_fragment_model")
public class FragmentModel extends BaseModel {
    /**
     * 别名
     */

    @TableField("alias")
    private String alias;


    /**
     * 片段编码
     */

    @TableField("code")
    private String code;


    /**
     * 模板id
     */

    @TableField("template_id")
    private String templateId;


    /**
     * 展示条数
     */

    @TableField("size")
    private Integer size;


    /**
     * 文件名称
     */

    @TableField("file_name")
    private String fileName;


    /**
     * 默认自带
     */

    @TableField("default_field_list")
    private String defaultFieldList;


    /**
     * 扩展字段
     */

    @TableField("extend_field_list")
    private String extendFieldList;


    /**
     * 所有选择字段（包含选择默认+扩展）
     */

    @TableField("all_field_list")
    private String allFieldList;


    /**
     * 必填字段
     */

    @TableField("required_field_list")
    private String requiredFieldList;


    /**
     * 字段对应中文名称
     */

    @TableField("field_text_map")
    private String fieldTextMap;


    /**
     * 创建用户id
     */


    /**
     * 修改人id
     */


    /**
     * 创建时间
     */


    /**
     * 修改时间
     */


}
