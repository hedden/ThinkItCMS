package com.thinkit.cms.entity.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 终端信息表
 * </p>
 *
 * @author LG
 * @since 2020-04-24
 */
@Data
@Accessors(chain = true)
@TableName("sys_oauth_client_details")
public class Clients extends BaseModel {


        @TableField("client_id")
        private String clientId;


        @TableField("resource_ids")
        private String resourceIds;



        @TableField("client_secret")
        private String clientSecret;



        @TableField("scope")
        private String scope;



        @TableField("authorized_grant_types")
        private String authorizedGrantTypes;



        @TableField("web_server_redirect_uri")
        private String webServerRedirectUri;



        @TableField("authorities")
        private String authorities;



        @TableField("access_token_validity")
        private Integer accessTokenValidity;



        @TableField("refresh_token_validity")
        private Integer refreshTokenValidity;



        @TableField("additional_information")
        private String additionalInformation;



        @TableField("autoapprove")
        private String autoapprove;

}
