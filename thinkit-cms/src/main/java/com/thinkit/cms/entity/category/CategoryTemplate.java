package com.thinkit.cms.entity.category;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分类-模板配置表
 * </p>
 *
 * @author LG
 * @since 2020-08-12
 */
@Data
@Accessors(chain = true)
@TableName("tk_category_template")
public class CategoryTemplate extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 分类ID
     */

    @TableField("category_id")
    private String categoryId;


    /**
     * 站点ID
     */

    @TableField("site_id")
    private String siteId;


    /**
     * 模板ID
     */

    @TableField("template_id")
    private String templateId;


    /**
     * 模板列表页路径
     */

    @TableField("template_path")
    private String templatePath;

}
