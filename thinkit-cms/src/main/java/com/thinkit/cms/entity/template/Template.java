package com.thinkit.cms.entity.template;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 模板表
 * </p>
 *
 * @author lg
 * @since 2020-07-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_template")
public class Template extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 模板名称
     */

    @TableField("name")
    private String name;


    @TableField("type_id")
    private String typeId;


    /**
     * 模板code
     */
    @TableField("template_code")
    private String templateCode;


    /**
     * 模板描述
     */
    @TableField("template_desc")
    private String templateDesc;


    /**
     * 售价
     */
    @TableField("price")
    private String price;


    /**
     * cover
     */
    @TableField("cover")
    private String cover;


    @TableField("is_share")
    private Boolean isShare;


    @TableField("source_folder")
    private String sourceFolder;


    /**
     * 模板压缩路径
     */
    @TableField("template_zip_path")
    private String templateZipPath;


    /**
     * 文件夹路径
     */
    @TableField("template_folder_path")
    private String templateFolderPath;


    /**
     * 文件大小
     */
    @TableField("file_size")
    private String fileSize;


    /**
     * 开发者名称
     */

    @TableField("developer_name")
    private String developerName;


    /**
     * 开发者 联系方式
     */

    @TableField("developer_contact")
    private String developerContact;


}
