package com.thinkit.cms.entity.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author dl
 * @since 2018-03-28
 */
@Data
@TableName("sys_online")
public class Online extends BaseModel {

    private static final long serialVersionUID = 1L;

    @TableField("table_name")
    private String tableName;

    @TableField("table_note")
    private String tableNote;

    private String engine;

    private String prefix;
    
    private String author;
    
    @TableField("file_override")
    private int fileOverride;
    
    private int open;
    
    private int resultmap;
    
    @TableField("column_list")
    private int columnList;
    
    
    @TableField("enable_cache")
    private int enableCache;

    @TableField("location")
    private String location;


    //模块名称
    @TableField("model_name")
    private String modelName;

    @TableField("super_controller_class")
    private String  superControllerClass;

    @TableField("super_service_class")
    private String  superServiceClass	;

    @TableField("super_service_impl_class")
    private String  superServiceImplClass	;

    @TableField("super_entity_class")
    private String  superEntityClass;

    @TableField("super_mapper_class")
    private String  superMapperClass;


    @TableField("base_pack")
    private String  basePack	;

    @TableField("controller_pack")
    private String  controllerPack	;

    @TableField("service_pack")
    private String  servicePack	;

    @TableField("service_impl_pack")
    private String  serviceImplPack	;

    @TableField("mapper_pack")
    private String  mapperPack	;

    @TableField("entity_pack")
    private String  entityPack	;



}
