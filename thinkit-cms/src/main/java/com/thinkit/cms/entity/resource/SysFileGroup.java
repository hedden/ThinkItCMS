package com.thinkit.cms.entity.resource;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * <p>
 * 
 * </p>
 *
 * @author sysFileGroup
 * @since 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_file_group")
public class SysFileGroup extends BaseModel {

    /**
     * 分组名称
     */

        @TableField("name")
        private String name;


    /**
     * 分组编码
     */

        @TableField("code")
        private String code;


    /**
     * 是否是系统分组，系统分组禁止删除
     */

        @TableField("is_sys_group")
        private Integer isSysGroup;


    /**
     * 排序
     */

        @TableField("sort")
        private Integer sort;


        /**
         * 站点ID
         */
        @TableField("site_id")
        private String siteId;


}
