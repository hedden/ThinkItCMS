package com.thinkit.cms.directives.method;

import cn.hutool.http.HtmlUtil;
import com.thinkit.directive.emums.LangEnum;
import com.thinkit.directive.emums.MethodEnum;
import com.thinkit.directive.render.BaseMethod;
import com.thinkit.utils.utils.Checker;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClearHtml extends BaseMethod {

    @Override
    public MethodEnum getName() {
        return MethodEnum.CLEAR_HTML;
    }

    @Override
    public Object exec(List list) throws TemplateModelException {
        String text = getString(0,list, LangEnum.STRING);
        if(Checker.BeNotBlank(text)){
            String tag = getString(1,list, LangEnum.STRING);
            if(Checker.BeNotBlank(tag)){
                String[] tags = tag.split("\\|");
                return HtmlUtil.unwrapHtmlTag(text,tags);
            }
            return HtmlUtil.cleanHtmlTag(text);
        }
        return "";
    }
}
