package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.dto.category.CategoryNavbar;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class NavbarDirective extends BaseDirective {

    @Autowired
    CategoryService categoryService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
             String code = render.getString("code");
             List<CategoryNavbar> trees = categoryService.navbar(code);
             render.put(getName().getCode(),trees).render();
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.NAVBAR;
    }
}
