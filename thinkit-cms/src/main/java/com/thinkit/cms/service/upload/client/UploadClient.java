package com.thinkit.cms.service.upload.client;

import cn.hutool.core.io.FileUtil;
import com.thinkit.cms.api.resource.SysResourceService;
import com.thinkit.cms.dto.resource.SysResourceDto;
import com.thinkit.cms.dto.upload.Chunk;
import com.thinkit.core.base.BaseContextKit;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.utils.SnowflakeIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.UUID;

public abstract class UploadClient {

    @Autowired
    SysResourceService resourceService;

    public abstract ApiResult uploadFile(String remoteFile);

    /**
     * 文件上传中触发
     * @param file
     * @return
     */
    public abstract ApiResult uploadFile(MultipartFile file);


    /**
     * 断点续传
     * @param chunk
     * @return
     */
    public abstract ApiResult keepUploadFile(Chunk chunk);

    /**
     * 文件上传前触发
     * @param file
     * @return
     */
    public abstract ApiResult readyUpload(MultipartFile file);

    /**
     * 文件上传成功触发
     * @param file
     * @return
     */
    public  abstract ApiResult uploadSuccess(ApiResult apiResult, MultipartFile file);

    /**
     * 文件上传失败触发
     * @param result
     * @return
     */
    public abstract void uploadError(ApiResult result);

    /**
     * 删除文件
     * @param params
     * @return
     */
    public abstract ApiResult deleteFile(Map<String,String> params);


    /**
     * 校验 文件是否存在
     * @param md5
     * @return
     */
    public abstract Map<String,Object>  checkFileIsExistByMd5(String md5);


    public ApiResult saveFileToDb(ApiResult resMap, MultipartFile file){
            Map<String,String> params= (Map<String, String>) resMap.get("res");
            String id = SnowflakeIdWorker.getId();
            String uid=UUID.randomUUID().toString();
            SysResourceDto resource = new SysResourceDto();
            String path = params.get("path");
            String group = params.get("group");
            String fileMd5=params.get("fileMd5");
            resource.setFileName(file.getOriginalFilename()).setFilePath(path).
            setFileFullPath(params.get("fileFullPath")).setGroupName(group).
            setFileSize(file.getSize()).setFileUid(uid).setFileMd5(fileMd5).
            setFileType(FileUtil.getSuffix(file.getOriginalFilename())).setCreateName(BaseContextKit.getAccount()).
            setId(id).setSiteId(BaseContextKit.getSiteId());
            resourceService.insert(resource);
            params.put("fileUid",uid);
            params.put("id",id);
            return ApiResult.result(params);
    }

    public ApiResult deleteFileByUid(String uid){
           resourceService.deleteByFiled("file_uid",uid);
           return  ApiResult.result();
    }

    public SysResourceDto getFileByUid(String uuid){
        return resourceService.getByField("file_uid",uuid);
    }

    public SysResourceDto checkerHasFileByMd5(String md5){
         SysResourceDto resource= resourceService.getByField("file_md5",md5);
         return resource;
    }


}
