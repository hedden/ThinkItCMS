package com.thinkit.cms.service.job.bean;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.dto.content.PublishDto;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class ContentPublishJob extends QuartzJobBean {

    @Autowired
    ContentService contentService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        PublishDto publishDto = (PublishDto)jobExecutionContext.getTrigger().getJobDataMap().get("bean");
        contentService.publish(publishDto,true);
    }
}
