package com.thinkit.cms.service.category;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.category.ChannelNotifyService;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.core.base.BaseContextKit;
import com.thinkit.core.constant.Channel;
import com.thinkit.core.constant.Constants;
import com.thinkit.directive.components.NotifyComponent;
import com.thinkit.nosql.base.BaseRedisService;
import com.thinkit.nosql.base.BaseSolrService;
import com.thinkit.nosql.enums.SolrCoreEnum;
import com.thinkit.processor.message.MessageSend;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class ChannelNotifyImpl implements ChannelNotifyService {

    @Autowired
    private MessageSend messageSend;

    @Autowired
    ContentService contentService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    BaseSolrService solrService;

    @Autowired
    NotifyComponent notifyComponent;

    @Autowired
    BaseRedisService baseRedisService;


    @Override
    public void notifyHomePage() {
        messageSend.sendMessage(ChannelEnum.HOME, Channel.NOTIFY_IT,true);
    }

    @Override
    public void notifyWholeSite() {
        notifyHomePage(); // 生成首页
        notifyCategoey(); // 栏目静态化
        notifyContent(1,1000); // 内容 静态化
    }

    @Override
    public void notifyDbToNoSql(Integer pageNo,Integer pageSize,Integer index) {
        if(pageNo==1){
            solrService.deleteByFiled(SolrCoreEnum.DEFAULT_CORE,"siteId",BaseContextKit.getSiteId());
        }
        IPage<ContentDto>  result = contentService.pageContentForNoSql(pageNo,pageSize);
        if(Checker.BeNotNull(result)){
            List<ContentDto> contentDtos = result.getRecords();
            Map<String,Object> params = new HashMap<>();
            for(ContentDto contentDto:contentDtos){
                boolean res = solrService.addContentRes(SolrCoreEnum.DEFAULT_CORE,contentDto);
                String title = contentDto.getTitle();
                if(Checker.BeNotBlank(title)){
                    title=title.length()>20?title.substring(0,20)+"...":title;
                }
                BigDecimal bindex = new BigDecimal(index);//24.698,24.702
                BigDecimal btotal = new BigDecimal(result.getTotal());
                String percent = bindex.divide(btotal,2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).toString();
                params.put(Channel.DEST_PATH,title);
                params.put(Channel.MSG,res? "添加成功!":"添加失败!");
                params.put(Channel.PERCENT,percent);
                params.put(Channel.CHANNER_NAME,"索引库同步");
                ApiResult apiResult = ApiResult.result(params);
                notifyComponent.notifyMsg(BaseContextKit.getUserId(),apiResult);
                index+=1;
            }
            pageNo+=1;
            notifyDbToNoSql(pageNo,pageSize,index);
        }
    }

    @Override
    public void flushdb() {
        baseRedisService.flushDB();
        Map<String,Object> params = new Hashtable<>();
        params.put("channerName","缓存清理");
        params.put("percent",100);
        params.put("destPath","OK");
        notifyComponent.notifyMsg(BaseContextKit.getUserId(), ApiResult.result(params));
    }

    private void notifyContent(Integer pageNo,Integer pageSize){
        Map<String,Object> result = contentService.pageContentParamsForAllGen(pageNo,pageSize);
        Boolean hasNext = (Boolean) result.get("hasNext") ;
        List<Map<String,String >> rows = (List<Map<String,String >>) result.get("rows");
        if(Checker.BeNotEmpty(rows)){
            List<String> ids = new ArrayList<>();
            for(Map<String,String > map:rows){
                ids.add(map.get(Constants.contentId));
            }
            if(Checker.BeNotEmpty(ids)){
                contentService.reStaticBatchGenCid(ids);
            }
        }
        if(hasNext){
            pageNo++;
            notifyContent(pageNo,pageSize);
        }
    }

    private void notifyCategoey(){
        List<String> ids =categoryService.getIds();
        if(Checker.BeNotEmpty(ids)){
            for(String id:ids){
                categoryService.genCategory(id);
            }
        }
    }
}
