package com.thinkit.cms.service.strategy;

import com.google.common.collect.Lists;
import com.thinkit.cms.api.strategy.StrategyService;
import com.thinkit.cms.dto.strategy.StrategyDto;
import com.thinkit.cms.entity.strategy.Strategy;
import com.thinkit.cms.mapper.strategy.StrategyMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.ActuatorEnum;
import com.thinkit.utils.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lg
 * @since 2021-05-12
 */
@Slf4j
@Transactional
@Service
public class StrategyServiceImpl extends BaseServiceImpl<StrategyDto, Strategy, StrategyMapper> implements StrategyService {



    @Override
    public List<StrategyDto> queryStrategy(String categoryId, String actionCode,Integer isSatrt) {
        List<StrategyDto> strategys = baseMapper.queryStrategy(categoryId,actionCode,isSatrt);
        return Checker.BeNotEmpty(strategys)?strategys: Lists.newArrayList();
    }

    @Transactional
    @Override
    public void saveStrategy(List<StrategyDto>strategyDtos) {
        if(Checker.BeNotEmpty(strategyDtos)){
            super.deleteByFiled("category_id",strategyDtos.get(0).getCategoryId());
            for(StrategyDto strategyDto:strategyDtos){
                strategyDto.setActionName(ActionEnum.getActionName(strategyDto.getActionCode()));
                strategyDto.setActuatorName(ActuatorEnum.getActuatorName(strategyDto.getActuatorCode()));
                strategyDto.setId(id());
            }
            super.insertBatch(strategyDtos);
        }
    }

    @Override
    public void clearStrategy(String categoryId) {
        super.deleteByFiled("category_id",categoryId);
    }
}
