package com.thinkit.cms.service.template;

import com.thinkit.cms.api.template.TemplateTypeService;
import com.thinkit.cms.dto.template.TemplateTypeDto;
import com.thinkit.cms.entity.template.TemplateType;
import com.thinkit.cms.mapper.template.TemplateTypeMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.core.handler.CustomException;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.Tree;
import com.thinkit.utils.utils.BuildTree;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 模板分类 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-07-28
 */
@Transactional
@Service
public class TemplateTypeServiceImpl extends BaseServiceImpl<TemplateTypeDto, TemplateType, TemplateTypeMapper> implements TemplateTypeService {


    @Override
    public Tree<TemplateTypeDto> selectTreeList() {
        List<TemplateTypeDto> typeDtos=baseMapper.listByPid(null);
        if(Checker.BeNotEmpty(typeDtos)){
            List<Tree<TemplateTypeDto>> trees = new ArrayList<Tree<TemplateTypeDto>>();
            for (TemplateTypeDto typeDto : typeDtos) {
                Tree<TemplateTypeDto> tree = new Tree<TemplateTypeDto>();
                tree.setId(typeDto.getId());
                tree.setKey(typeDto.getId());
                tree.setValue(typeDto.getId());
                tree.setParentId(typeDto.getParentId());
                tree.setName(typeDto.getName());
                tree.setTitle(typeDto.getName());
                Map<String,Object> map = new HashMap<>();
                map.put("gmtCreate",typeDto.getGmtCreate());
                map.put("code",typeDto.getCode());
                tree.setAttributes(map);
                trees.add(tree);
            }
            Tree<TemplateTypeDto> t = BuildTree.build(trees);
            return t;
        }else{
            return null;
        }
    }

    @Override
    public TemplateTypeDto getById(String id) {
        return baseMapper.getById(id);
    }

    @Override
    public void save(TemplateTypeDto v) {
        ckHasCode(null, v.getCode());
        super.insert(v);
    }

    @Override
    public void update(TemplateTypeDto v) {
        ckHasCode(v.getId(), v.getCode());
        super.updateByPk(v);
    }

    @Override
    public boolean delete(String id) {
        TemplateTypeDto typeDto= getByPk(id);
        List<String> ids =new ArrayList<>();
        ids.add(id);
        if(!"0".equals(typeDto.getParentId())){
            TemplateTypeDto  ptypeDto= getByPk(typeDto.getParentId());
            if(Checker.BeNotNull(ptypeDto)){
                ids.add(ptypeDto.getId());
            }
        }else{
            baseMapper.deleteByPid(typeDto.getId());
        }
        return deleteByPks(ids);
    }

    @Override
    public List<TemplateTypeDto> getTempByPidTypes(String pid) {
        return baseMapper.listByPid(pid);
    }

    @Override
    public TemplateTypeDto getByCode(String code) {
        return baseMapper.getByCode(code);
    }

    private void ckHasCode(String id,String code){
        Integer count =baseMapper.ckHasCode(id,code);
        if(count>0){
            throw new CustomException(ApiResult.result(5020));
        }
    }
}
