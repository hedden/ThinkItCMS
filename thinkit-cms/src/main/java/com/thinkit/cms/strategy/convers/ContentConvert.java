package com.thinkit.cms.strategy.convers;

import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.strategy.filter.TmpActuator;
import com.thinkit.core.constant.Channel;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ContentConvert {



    public List<Map<String,Object>>  convertParam(List<ContentDto> contents){
        List<Map<String,Object>> maps = new ArrayList<>();
        if(Checker.BeNotEmpty(contents) ){
            for(ContentDto content:contents){
                Map<String,Object> map = new HashMap<>();
                map.put(Channel.CATEGORY_ID,content.getCategoryId());
                map.put(Channel.MODEL_ID,content.getModelId());
                map.put(Channel.CONTENT_ID,content.getId());
                maps.add(map);
            }
        }
        return maps;
    }


    public Map<String,Object> convertParam(Object params, ActionEnum actionEnum){
        Map<String,Object> map = new HashMap<>();
        if(Checker.BeNotNull(params)){
            if(ActionEnum.CREATE_CONTENT.equals(actionEnum) ||
                ActionEnum.UPDATE_CONTENT.equals(actionEnum) ||
                ActionEnum.TOP_CONTENT.equals(actionEnum)){ // 创建,更新,置顶
                ContentDto content = (ContentDto) params;
                map.put(Channel.CATEGORY_ID,content.getCategoryId());
                map.put(Channel.MODEL_ID,content.getModelId());
                map.put(Channel.CONTENT_ID,content.getId());
            }
        }
        return map;
    }

    public List<Map<String,Object>> convertParam(Object params, ActionEnum actionEnum,TmpActuator actuator){
        List<Map<String,Object>> maps = new ArrayList<>();
        if(Checker.BeNotNull(params)){
            if( ActionEnum.CREATE_CONTENT.equals(actionEnum) ||
                ActionEnum.UPDATE_CONTENT.equals(actionEnum) ||
                ActionEnum.TOP_CONTENT.equals(actionEnum)){ // 创建,更新,置顶
                Map<String,Object> map = new HashMap<>();
                ContentDto content = (ContentDto) params;
                map.put(Channel.CATEGORY_ID,content.getCategoryId());
                map.put(Channel.MODEL_ID,content.getModelId());
                map.put(Channel.CONTENT_ID,content.getId());
                maps.add(map);
            }else if(ActionEnum.PUBLISH_CONTENT.equals(actionEnum)){ // 发布内容
                Object object = actuator.getChecker().getParam();
                if(Checker.BeNotNull(object) && object instanceof List){
                    List<String> cids =  (List) object;
                    for(String cid:cids){
                        Map<String,Object> map = new HashMap<>();
                        map.put(Channel.CATEGORY_ID,params);
                        map.put(Channel.CONTENT_ID,cid);
                        maps.add(map);
                    }
                }
            }
        }
        return maps;
    }


}
