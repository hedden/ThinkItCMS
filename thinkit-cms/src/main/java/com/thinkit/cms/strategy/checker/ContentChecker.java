package com.thinkit.cms.strategy.checker;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.entity.content.Content;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.StatusEnum;
import com.thinkit.utils.utils.Checker;
import lombok.Getter;
import lombok.Setter;

public class ContentChecker implements ActuatorCheck {

    @Getter @Setter
    private ContentDto content;

    @Getter @Setter
    private ActionEnum actionEnum;


    public ContentChecker (Object arg){
        if(Checker.BeNotNull(arg)){
            ContentDto content = (ContentDto) arg;
            this.content =content;
        }
    }

    @Override
    public boolean executeFilter(Object arg) {
        if(Checker.BeNotNull(content)){
            if(actionEnum.equals(ActionEnum.UPDATE_CONTENT)){ // 修改时的判断依据
                return StatusEnum.PUBLISH.getCode().equals(content.getStatus());
            }else if(actionEnum.equals(ActionEnum.DELETE_CONTENT)){// 删除时的判断依据
                return StatusEnum.PUBLISH.getCode().equals(content.getStatus());
            }
        }
        return false;
    }

    @Override
    public void setAction(ActionEnum actionEnum) {
        this.actionEnum = actionEnum;
    }

    @Override
    public Object getParam() {
        return content;
    }
}
