package com.thinkit.cms.strategy.filter;

import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.dto.strategy.StrategyDto;
import com.thinkit.cms.strategy.checker.ActuatorCheck;
import com.thinkit.cms.strategy.convers.CategoryConvert;
import com.thinkit.processor.message.MessageSend;
import com.thinkit.utils.properties.ThinkItProperties;
import com.thinkit.utils.utils.Checker;
import com.thinkit.utils.utils.SpringContextHolder;
import org.quartz.JobExecutionContext;

/**
 * @author lg
 * 责任链处理 特效
 */
public abstract class TmpActuator extends ActuatorFilter {

    protected MessageSend messageSend;

    protected CategoryService categoryService;

    protected ContentService contentService;

    protected ThinkItProperties thinkItProperties;

    protected ActuatorCheck actuatorCheck;

    protected StrategyDto strategyDto;

    public TmpActuator(){
        this.messageSend = SpringContextHolder.getBean(MessageSend.class);
        this.categoryService = SpringContextHolder.getBean(CategoryService.class);
        this.thinkItProperties = SpringContextHolder.getBean(ThinkItProperties.class);
        this.contentService = SpringContextHolder.getBean(ContentService.class);
    }

    public void setChecker(ActuatorCheck actuatorCheck){
        this.actuatorCheck = actuatorCheck;
    }

    public ActuatorCheck getChecker(){
       return this.actuatorCheck ;
    }

    public Filter setStrategy(StrategyDto strategyDto){
        this.strategyDto = strategyDto;
        return this;
    }

    public String triggerTime(){
        if(Checker.BeNotNull(strategyDto)){
            return strategyDto.getTriggerTime();
        }
        return null;
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext)  {

    }

    protected boolean isNotJob(){
        if(Checker.BeNotNull(strategyDto)){
            return "now".equals(strategyDto.getTriggerTime());
        }
        return false;
    }

    protected boolean ok(Object params){
        if(Checker.BeNotNull(actuatorCheck)){
            return actuatorCheck.executeFilter(params);
        }else{
            return true;
        }
    }
}
