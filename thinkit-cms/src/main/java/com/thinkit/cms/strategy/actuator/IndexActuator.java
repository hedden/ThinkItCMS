package com.thinkit.cms.strategy.actuator;
import com.thinkit.cms.strategy.filter.FilterChain;
import com.thinkit.cms.strategy.filter.TmpActuator;
import com.thinkit.core.annotation.Actuator;
import com.thinkit.core.base.BaseContextKit;
import com.thinkit.core.constant.Channel;
import com.thinkit.processor.channel.ChannelThreadLocal;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

/**
 * 首页模板生成执行器
 */
@Actuator(code ="INDEX_ACTUATOR",name = "首页模板生成执行器")
public class IndexActuator extends TmpActuator  {

    public IndexActuator(){
        super();
    }

    public IndexActuator(Object params,ActionEnum actionEnum){
       super();
       this.params = params;
       this.actionEnum=actionEnum;
    }

    @Getter @Setter
    private Object params;

    @Getter @Setter
    private ActionEnum actionEnum;

    @Override
    public void doFilter( Object params, FilterChain chain) {
        if(Checker.BeNull(params)){
            params = this.params;
        }
        if(ok(params)){
            if(isNotJob()) {
                messageSend.sendMessage(ChannelEnum.HOME, Channel.NOTIFY_IT);
            }else{
                chain.addJobFilter(this);
            }
        }
        chain.doFilter(params,chain);
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext)  {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        String siteId = jobDataMap.get(Channel.SITE_ID).toString();
        String userId = jobDataMap.get(Channel.USER_ID).toString();
        Object param = jobDataMap.get("param");
        ChannelThreadLocal.set(Channel.SITE_ID,siteId);
        BaseContextKit.setSiteId(siteId);
        BaseContextKit.setUserId(userId);
        messageSend.sendMessage(ChannelEnum.HOME, Channel.NOTIFY_IT,false);
    }


    public Object getParam(){
        return this.params;
    }
}
